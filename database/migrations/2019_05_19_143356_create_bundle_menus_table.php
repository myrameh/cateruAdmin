<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBundleMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bundle_menus', function (Blueprint $table) {
            $table->increments('bundleid',4001);
            $table->string('name',100);
            $table->text('details');
            $table->float('price');
            $table->integer('servingsize');
            $table->integer('menuID')->unsigned();
            //$table->timestamps();

            $table->foreign('menuID')->references('menuID')->on('menus');
    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bundle_menus');
    }
}
