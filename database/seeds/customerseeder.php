<?php

use Illuminate\Database\Seeder;

class customerseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $customers = [
            [
                'name'  => 'Maria',
                'phonenumber' => '09479842734'
            ],
        ];
        DB::table('customers')->insert($customers);
    }
}
