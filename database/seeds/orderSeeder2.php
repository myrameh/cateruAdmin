<?php

use Illuminate\Database\Seeder;

class orderSeeder2 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $orders = [
            [
                'custid'    => 7,
                'username'  => 'lalaluna',
                'tableno'   => 3,
                'total'     =>0,
                'status'    => 'ordering'
            ],
        ];
        DB::table('orders')->insert($orders);
    }
}
