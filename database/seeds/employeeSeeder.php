<?php

use Illuminate\Database\Seeder;

class employeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $employees= [
            [
                'empfirstname' =>  'Helvi',
                'emplastname'  =>  'Tagacanao',
                'username'  =>  'waiterhelvi',
                'password'  =>  bcrypt('123456'),
                'position'  => 'waiter'
            ],
           
        ];
        DB::table('employees')->insert($employees);
    }
}
