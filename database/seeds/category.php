<?php

use Illuminate\Database\Seeder;

class category extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
        
            [

                'categoryname'=> 'Soup',
                'description' => 'Native Soups'
            ]
        ];
        DB::table('categories')->insert($categories);
    }
}
