<?php

use Illuminate\Database\Seeder;

class TableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tables = [
            [
                
                'capacity'=> 4,
                'status'    => 'Available'
            ],
            [
                
                'capacity'=> 6,
                'status'    => 'Occupied'
            ],
            [
               
                'capacity'=> 2,
                'status'    => 'Available'
            ]
        
        ];
        DB::table('tables')->insert($tables);
    }
}
