<?php

use Illuminate\Database\Seeder;

class orderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $orders = [
            [
                'order_id'    => 5,
                'orderQty'  =>  2,
                'menuID'    =>  2,
                'status'    =>  'waiting',
                'subtotal'  =>  200.00
            ],
        ];
        DB::table('order_details')->insert($orders);
    }
}
