<?php

use Illuminate\Database\Seeder;

class subcategory extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $subcategories = [

            [
                'subname'   => 'PORK',
                'categoryid'=> 7
            ],
            [
                'subname'   => 'BEEF',
                'categoryid'=> 7
            ],
            [
                'subname'   => 'FISH',
                'categoryid'=> 7
            ],
            [
                'subname'   => 'SEAFOOD',
                'categoryid'=> 7
            ],


        ];
        DB::table('sub_categories')->insert($subcategories);
    }
}
