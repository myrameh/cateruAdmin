<?php

use Illuminate\Database\Seeder;

class menu extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menus = [
           // [
                // 'menuID' => 3001,
                // 'name'  => 'Spaghetti',
                // 'details'=> 'This is not jollibee spaghetti',
                // 'price' => 100.00,
                // 'servingsize' => 3,
                // 'image'=> null,
            //     // 'subcatid'=> 2003
            //     'name'  => 'Birds Nest',
            //     'details'=> 'Chicken and quail eggs',
            //     'price' => 120.00,
            //     'servingsize' => 2,
            //     'image'=> null,
            //     'subcatid'=> 2
            // ],
            
            [
                
                'name'  => 'Utan Bisaya',
                'details'=> 'The healthy soup filled with deliecious native vegetables',
                'price' => 115.00,
                'servingsize' => 2,
                'image'=> null,
                'subcatid'=> 1
            ],
        ];
        DB::table('menus')->insert($menus);
    }
}
