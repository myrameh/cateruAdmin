@extends('layouts.loginlayout')

@section('content')
		 <div class="container h-p100">
				<div class="login-container">
					<div class="right-login">
							<h3 class="text-center">caterU</h3>
							<form action="{{ url('/login/authenticate')}}" method="post" class="form-group">
								{{ csrf_field() }}
								<div class="form-group inputWithImg">
									<input type="text" class="form-control" name="username" placeholder="Username" >
								<img src="{{asset('/assets/svg/person.svg')}}" >
							</div>
								<div class="form-group inputWithImg">
									<input type="password" class="form-control" name="password" placeholder="Password">
								<img src="{{asset('/assets/svg/lock-locked.svg')}}" >
								@if($errors->any())
								<span class="help-block">
									<strong style="color:pink">{{$errors->first()}}</strong>
								</span>
								
								@endif
								</div>
								<div class="row">
								<div class="col-6">
									<div class="checkbox">
									<input type="checkbox" id="basic_checkbox_1">
									<label for="basic_checkbox_1">Remember Me</label>
									</div>
								</div>
								<!-- /.col -->
								<div class="col-6">
								<div class="fog-pwd text-right">
									<a href="javascript:void(0)" class="text-white"><i class="ion ion-locked"></i> Forgot password?</a><br>
									</div>
								</div>
								<!-- /.col -->
								<div class="col-12 text-center">
									<button type="submit" class="btn btn-info btn-block margin-top-10">SIGN IN</button>
								</div>
								<!-- /.col -->
								</div>
							</form>
							<div class="margin-top-30 text-center">
									<p>Don't have an account? <a href="{{url('/createaccount')}}" class="text-info m-l-5">Sign Up</a></p>
								</div>
						</div>
					</div>
				</div>
			
@endsection
@section('onchange')
<script>
$('.message a').click(function(){
	$('form').animate({height: "toggle", opacity: "toggle"}, "slow");
});
</script>
@endsection