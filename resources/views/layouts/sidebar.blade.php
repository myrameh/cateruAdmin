<nav id="sidebar">
        <div class="sidebar-header">
            <h3>CaterU</h3>
        </div>

        <ul class="list-unstyled components">
            <li><a href="{{ url('/dashboard')}}" style="color:white">Dashboard</a></li>
            <li>
                <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <i class="fa fa-home"></i>
                    Menu
                </a>
                <ul class="collapse list-unstyled" id="homeSubmenu">
                    <li >
                        <a href="{{url('/menu/list?mode=list')}}">Menu List</a>
                    </li>
                    <li>
                        <a href="{{ url('/menu/addmenu')}}">Add New Menu</a>
                    </li>
                    <li>
                        <a href="{{ url('/admin/view_categories')}}">Category List</a>
                    </li>
                    <li>
                        <a href="{{ url('/admin/add_category')}}">Add New Category</a>
                    </li>
                    <li>
                        <a href="{{ url('/admin/add_subcategory')}}">Add New SubCategory</a>
                    </li>
                    <li>
                        <a href="{{ url('/admin/view_subcategories')}}">SubCategory List</a>
                    </li>

                </ul>
            </li>
            <li>
                <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <i class="fa fa-copy"></i>
                    Report
                </a>
                <ul class="collapse list-unstyled" id="pageSubmenu">
                    <li>
                        <a href="{{url('/orders/successfulTransaction')}}">Sales</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="{{ url('/table/tablelist')}}" style="color:white"><i class="fa fa-user"></i>Tables</a>
            </li>
        </ul>
        @include('layouts.aside')
</nav>

