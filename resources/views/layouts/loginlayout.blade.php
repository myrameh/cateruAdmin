<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../images/favicon.ico">

    <title>CaterU</title>
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap-extend.css')}}">
    <link rel="stylesheet" href="{{asset('css/master-style.css')}}">
    <link rel="stylesheet" href="{{asset('css/skin.css')}}">
    <link rel="stylesheet" href="{{ asset('css/login.css')}}">
</head>

<body class="hold-transition login-page" >
        @yield('content')
    <!-- Jquery JSfgf-->
    <script src="{{asset('js/jquery.min.js')}}"></script>
        
    <!-- popper -->
    <script src="../../assets/vendor_components/popper/dist/popper.min.js"></script>
    
    <!-- Bootstrap 4.0-->
    <script src="../../assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>
    
    <!-- SlimScroll -->
    <script src="../../assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    
    <!-- FastClick -->
    <script src="../../assets/vendor_components/fastclick/lib/fastclick.js"></script>
    
    <!-- BonitoPro Admin App -->
    <script src="../js/template.js"></script>

</body>

</html>
<!-- end document-->