@extends('mainlayout')
@section('content')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item" style="color:black">Employee</li>
      <li class="breadcrumb-item active" aria-current="page">Employee List</li>
    </ol>
</nav>
        <div class="col-12">
          <div class="box">
            <div class="box-header with-border">
              <div class="box-controls pull-right">
				          <div class="lookup lookup-circle lookup-right">
                    <a href="{{url('/employee/addemployee')}}">
                    <i class="fa fa-plus"></i>Add new employee</a>
        </div>
       <hr>
			  </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
				<div class="table-responsive">
				  <table class="table table-hover">
            <thead class="thead-dark">
					<tr>
					  <th>Employee Name</th>
					  <th>Position</th>
					  <th>Action</th>
          </tr>	
        </thead>
        @foreach($lists as $list)
            <tr>
            <td><a href="javascript:void(0)">{{ $list->empfirstname.' '. $list->emplastname}}</a></td>
            <td>{{ $list->position}}</td>

            <td>
                <a href="{{ url('/employee/'.$list->username.'/edit') }}"><img src="{{ asset('/assets/svg/pencil.svg') }}" alt="" width="20px" height="20px"></a>&emsp;
                <a href="{{ url('/employee/'. $list->username.'/delete') }}"><img src="{{ asset('/assets/svg/trash.svg') }}" data-menuid="5" alt="" width="20px" height="20px" ></a>&emsp;
                
            </td>	
          </tr>
        @endforeach 
      </table>
                </div>
           
@endsection
