@extends('mainlayout')
@section('content')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item" style="color:black"><b>Table</b></li>
      <li class="breadcrumb-item active" aria-current="page" style="color:blue;font-weight:bold;">Table List</li>
    </ol>
</nav>
        <div class="col-12">
            <div class="card">
                <div class="card-body">
          <div class="box">
            <div class="box-header with-border">
              <div class="box-controls pull-right">
				          <div class="lookup lookup-circle lookup-right">
                    <a href="{{url('/table/addtable')}}">
                    <i class="fa fa-plus"></i>Add more table</a>
        </div>
       <hr>
			  </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
				<div class="table-responsive">
				  <table class="table table-hover">
            <thead class="thead-dark">
					<tr>
					  <th>Table No</th>
                      <th>Capacity</th>
                      <th>Status</th>
					  <th>Action</th>
          </tr>
        </thead>
        @foreach($allTables as $table)
            <tr>
                <td>{{ $table->tableno}}</td>
                <td>{{ $table->capacity}}</td>
                <td>{{ $table->status}}</td>

            <td>
                <a href="{{ url('/table/'.$table->tableno.'/edit') }}"><img src="{{ asset('/assets/svg/pencil.svg') }}" alt="" width="20px" height="20px"></a>&emsp;
                <a href="{{ url('/table/'. $table->tableno.'/delete') }}"><img src="{{ asset('/assets/svg/trash.svg') }}" data-menuid="5" alt="" width="20px" height="20px" ></a>&emsp;

            </td>
          </tr>
        @endforeach
      </table>
    </div>
</div>
                </div>

@endsection
