@extends('mainlayout')
@section('content')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item" style="color:black"><b>Menu</b></li>
      <li class="breadcrumb-item active" aria-current="page"  style="color:blue; font-weight:bold">Category List</li>
    </ol>
</nav>
<div class="col-12">
    <div class="box"  >
        <div class="card">
                <div class="card-body">
    <div class="box-header with-border">
        <div class="box-controls pull-right">
        <div class="lookup lookup-circle lookup-right">
            <a href="{{url('/admin/add_category')}}">
                <i class="fa fa-plus"></i> Add New Category</a>
        </div>
        <hr>
			  </div>
            </div>
            <!-- /.box-header -->

                    <div class="box-body no-padding">
				<div class="table-responsive">
				  <table class="table table-hover">
            <thead class="thead-dark">
					<tr>
					  <th>Category ID</th>
					  <th>Name</th>
					  <th>Decription</th>
					  <th>Action</th>
          </tr>
        </thead>
        @foreach($allCategories as $category)
            <tr>
            <td><a href="javascript:void(0)">{{ $category->categoryid}}</a></td>
            <td>{{ $category->categoryname}}</td>
            <td>{{$category->description}}</td>
            <td>
                <a href="{{ url('/admin/edit_category/'.$category->categoryid) }}"><img src="{{ asset('/assets/svg/pencil.svg') }}" alt="" width="20px" height="20px"></a>&emsp;
                <a href="{{ url('/admin/delete_category/'. $category->categoryid) }}"><img src="{{ asset('/assets/svg/trash.svg') }}" data-menuid="5" alt="" width="20px" height="20px" ></a>&emsp;

            </td>
        </tr>
        @endforeach
      </table>
                </div>
                </div>
            </div>


@endsection
