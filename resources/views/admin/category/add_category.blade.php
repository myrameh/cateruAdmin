@extends('mainlayout')

@section('content')
<nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item" style="color:black"><b>Menu</b></li>
          <li class="breadcrumb-item active" aria-current="page" style="color:blue;font-weight:bold">Add New Category</li>
        </ol>
    </nav>
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
               <div class="card">
                   {{-- <div class="card-body ">
                        <a href="{{url('/admin/add_category')}}">
                           Add new category <img src="{{ asset('/assets/svg/arrow-right.svg') }}" alt="" width="20px" height="20px"></a>&emsp;</a>
                   </div> --}}

                   <div class="card-body" id="form-group">
                       <div class="box-body" style="width:50%">
                        <form class="form-group form-horizontal"action={{ url('/admin/add_category')}} method="post" name="add_category" id="add_category" novalidate="novalidate">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label>Category name</label>
                                <input type="text" class="form-control" name="categoryname" id="categoryname"
                                value="{{ old('categoryname')}}" placeholder="Category Name ">
                            </div>
                            <div class="form-group" >
                                <label for="">Description</label>
                                <textarea class="form-control"  name="description" placeholder="Enter..."value="{{old('description')}}"></textarea>
                            </div>
                            <div class="form-actions">
                                <input type="submit" value="Add Category" class="btn btn-success">
                            </div>
                        </form>
                    </div>
                   </div>
               </div>

                </div>
            </div>
        </div>
    </div>



@endsection


