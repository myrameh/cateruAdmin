@extends('mainlayout')

@section('content')
<div class="col-md-9">				
        <div class="box">   
            <div class="box-header">
            <h4 class="box-title">Sales Graph</h4>
            </div> 
          <div class="row d-flex justify-content-center">
          <!--Grid column-->
          <div class="col-md-12">
            <canvas id="lineChart"></canvas>
          </div>
          <!--Grid column-->
      
        </div>
        </div>
</div>
       
        <!--Grid row-->
       
        <!--Grid row-->
      
      </div>
@endsection
@section('onchange')
<script type="text/javascript">
var ctxL = document.getElementById("lineChart").getContext('2d');
var gradientFill = ctxL.createLinearGradient(0, 0, 0, 290);
    gradientFill.addColorStop(0, "rgba(173, 53, 186, 1)");
    gradientFill.addColorStop(1, "rgba(173, 53, 186, 0.1)");
var myLineChart = new Chart(ctxL, {
  type: 'bar',
  data: {
    labels: ["January", "February", "March", "April", "May", "June", "July","August",'September','October','November','December'],
    datasets: [
      {
        label: "My First dataset",
        data: [0, 65, 45, 65, 35, 65, 0,35,45,65,1,10],//TOTAL BASED ON MONTH, WEEK AND YEAR
        backgroundColor: gradientFill,
        borderColor: [
          '#AD35BA',
        ],
        borderWidth: 2,
        pointBorderColor: "#fff",
        pointBackgroundColor: "rgba(173, 53, 186, 0.1)",
      }
    ]
  },
  options: {
    responsive: true
  }
});
</script>
@endsection
