@extends('layouts.loginlayout')

@section('content')
<div class="container h-p100">
		<div class="register-container">
				<div class="right-register">
						<h3 class="text-center">caterU</h3>
					<form action="{{ url('/createaccount')}}" method="post" class="form-group">
						{{ csrf_field()}}
                        <div class="form-group">
                            <input type="text" class="form-control" name="empfirstname" placeholder="Firstname" value="{{ old('empfirstname')}}">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="emplastname" placeholder="Lastname" value="{{ old('emplastname')}}">
                        </div>
                        <div class="form-group">
                            <label for="">Position</label>
                            <select name="position" class="form-control">
                                <option value="admin">Admin</option>
                                <option value="manager" >Manager</option>
                                <option value="cashier">Cashier</option>
                                <option value="receptionist">Receptionist</option>
                                <option value="kitchen staff">Kitchen Staff</option>
                                <option value="waiter">Waiter</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="username" placeholder="Username" value="{{ old('username')}}">
                        </div>
					  <div class="form-group">
						<input type="password" class="form-control"name="password" placeholder="Password" value="{{ old('password')}}">

					  </div>
					  <div class="row ">

						<div class="col-12 text-center">
						  <button type="submit" class="btn btn-info btn-block margin-top-10">Sign Up</button>
						</div>
						<!-- /.col -->
					  </div>
                    </form>
               </div>

					<div class="margin-top-30 text-center">
						<p>Have an account? <a href="{{url('/login')}}" class="text-info m-l-5">Sign In</a></p>
					</div>
				</div>
			</div>
		</div>
@endsection
