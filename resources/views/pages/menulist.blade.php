@extends('mainlayout')
@section('content')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item" style="color:black"><b>Menu</b></li>
      <li class="breadcrumb-item active" aria-current="page"  style="color:blue; font-weight:bold">Menu List</li>
    </ol>
</nav>
<div class="card">
    <div class="card-body">
        <div class="col-12">
          {{-- <div class="box">
            <div class="box-header with-border">
              <div class="box-controls pull-right">
                    <a href="{{url('/menu/addmenu')}}">
                    <i class="fa fa-plus"></i> Add new menu</a>
                </div>
                  </div>
			  </div> --}}
            </div>

            <!-- /.box-header -->
            <div class="box-body no-padding">
				<div class="table-responsive">
                    <div class="separator">&nbsp;</div>
                    <div class="card">
                        <div class="card-body">
                             <table class="table table-hover">
            <thead class="thead-dark">
					<tr>
                        <th>Images</th>
					    <th>Menu ID</th>
					    <th>Name</th>
					    <th>Price</th>
					    <th>Action</th>
          </tr>
        </thead>
        @foreach($allMenus as $menu)
            <tr>
            <td><img src="{{asset('menu/menu_images/'.$menu->image)}}"  style="width:20px;height:20px;"
             ></td>
            <td>{{ $menu->menuID}}</td>
            <td>{{ $menu->name}}</td>
            <td>{{$menu->price}}</td>
            <td>
                <a href="{{ url('/menu/'.$menu->menuID.'/edit') }}"><img src="{{ asset('/assets/svg/pencil.svg') }}" alt="" width="20px" height="20px"></a>&emsp;
                <a href="{{ url('/menu/'.$menu->menuID.'/delete') }}"><img src="{{ asset('/assets/svg/trash.svg') }}" alt="" width="20px" height="20px"></a>&emsp;

                {{-- <a href="{{ url('/menu/'. $menu->menuID.'/delete') }}"><img src="{{ asset('/assets/svg/trash.svg') }}" data-menuid="5" alt="" width="20px" height="20px" ></a>&emsp; --}}

            </td>
        </tr>
        @endforeach
      </table>
                        </div>
                    </div>

                </div>

@endsection
