@extends('mainlayout')

@section('content')

<div class="box">
        <div class="box-header with-border">
          <h4 class="box-title">Add New Menu</h4>
        </div>
        <!-- /.box-header -->
        <div class="card">
                <div class="card-body">
        <div class="box-body">
          <form method="post" action="{{ url('/menu/'.$menuRecord->menuID.'/edit')}}" enctype="multipart/form-data">
            @csrf
            <!-- text input -->
            <div class="form-group">
                <label for="" class="control-label">Upload Image</label>
                <input type="file" name="image" value="{{ old('image')}}">
              </div>
            <div class="form-group">
                    <label>Menu ID</label>
                    <input disabled type="text" name="name" class="form-control" placeholder="Enter ..." value="{{ $menuRecord->menuID}}">
                  </div>
            <div class="form-group">
              <label>Menu name</label>
              <input type="text" name="name" class="form-control" placeholder="Enter ..." value="{{ $menuRecord->name}}">
            </div>

            <div class="form-group">
              <label>Detail</label>
              <textarea class="form-control" rows="3"  name="details" placeholder="Enter ...">{{ $menuRecord->details}}</textarea>
            </div>
            <div class="form-group">
                <label for="">Price</label>
                <input type="number" class="form-control" name="price" placeholder="Enter..." value={{ $menuRecord->price }}>
            </div>
            <div class="form-group">
                    <label for="">Serving size</label>
                    <input type="number" class="form-control"  name="servingsize" placeholder="Enter..." value="{{ $menuRecord->servingsize}}">
            </div>

            {{-- <div class="form-group">
              <label>Category</label>
              <select name="categoryid" class="form-control">
                @foreach($allCategories as $category)
                <option value="{{$category->categoryid}}">{{ $category->categoryname}}</option>
               @endforeach
              </select>
            </div>
            <div class="form-group">
                    <label>SubCategory</label>
                    <select class="form-control" name="subcatid">
                      @foreach($allSubCategories as $sub)
                      <option value="{{$sub->subcatid}}">{{ $sub->subname}}</option>
                     @endforeach
                    </select>
                  </div> --}}
                  <div class="form-group">
                        <label>Category</label>
                        <select name="categoryid" id="category" class="form-control">
                        <option value="">Select Category</option>
                            @foreach ($allCategories as $category)
                            <option value={{ $category->categoryid}}>{{ $category->categoryname}}</option>
                        @endforeach
                        </select>
                    </div>
                        <div class="form-group">
                            <label>Subcategory</label>
                            <select name="subcategory" id="subcategory" class="form-control" >
                            <option value=""></option>
                            </select>
                        </div>

            <div class="card-footer">
                <button type="submit" class="btn btn-primary btn-sm">
                    <i class="fa fa-dot-circle-o"></i> Submit
                </button>
                <button type="reset" class="btn btn-danger btn-sm">
                    <i class="fa fa-ban"></i> Cancel
                </button>
            </div>
          </form>
        </div>
    </div>
</div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!--/.col (right) -->
    </div>
  </div>
@endsection
@section('onchange')

<script type="text/javascript">

$(document).ready(function(){

  $('#subcategory').parent().hide();
  $('#category').change(function(e){
    var cat_id = e.target.value;
    $('#subcategory').empty();
  $.get("/menu/category?categoryid="+cat_id, function(data){
      $.each(data.subs, function(index, subcategory) {
        $('#subcategory').append('<option value="'+subcategory.subcatid+'">'+subcategory.subname+'</option>').parent().show();
      });
  });

  });
});

</script>
@endsection
