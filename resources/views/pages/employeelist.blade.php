@extends('layouts.mainlayout')

@section('maincontent')
<div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <div class="overview-wrap">
                    <h2 class="title-1">Employee List</h2>
                <a href="{{url('/addemployee')}}"><button class="au-btn au-btn-icon au-btn--blue">
                        <i class="zmdi zmdi-plus"></i>Add New Employee</button></a>
                </div>
            </div>
        </div>
        <hr>
        <table class="table table-borderless table-data3">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Position</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>2018-09-29 05:57</td>
                    <td>Mobile</td>
                    <td>iPhone X 64Gb Grey</td>
                    <td><a href=""><i class="fas fa-edit" data-toggle="tooltip" title="Edit"></i></a>&emsp;
                        <a href=""><i class="fas fa-trash-alt" data-toggle="tooltip" title="Delete"></i></a>
                    </td>
                </tr>
                <tr>
                    <td>2018-09-29 05:57</td>
                    <td>Mobile</td>
                    <td>iPhone X 64Gb Grey</td>
                    <td><a href=""><i class="fas fa-edit" data-toggle="tooltip" title="Edit"></i></a>&emsp;
                        <a href=""><i class="fas fa-trash-alt" data-toggle="tooltip" title="Delete"></i></a>
                    </td>
                </tr>
                <tr>
                    <td>2018-09-29 05:57</td>
                    <td>Mobile</td>
                    <td>iPhone X 64Gb Grey</td>
                    <td><a href=""><i class="fas fa-edit" data-toggle="tooltip" title="Edit"></i></a>&emsp;
                        <a href=""><i class="fas fa-trash-alt" data-toggle="tooltip" title="Delete"></i></a>
                    </td>
                </tr>
                <tr>
                    <td>2018-09-29 05:57</td>
                    <td>Mobile</td>
                    <td>iPhone X 64Gb Grey</td>
                    <td><a href=""><i class="fas fa-edit" data-toggle="tooltip" title="Edit"></i></a>&emsp;
                        <a href=""><i class="fas fa-trash-alt" data-toggle="tooltip" title="Delete"></i></a>
                    </td>
                </tr>
                <tr>
                    <td>2018-09-29 05:57</td>
                    <td>Mobile</td>
                    <td>iPhone X 64Gb Grey</td>
                    <td><a href=""><i class="fas fa-edit" data-toggle="tooltip" title="Edit"></i></a>&emsp;
                        <a href=""><i class="fas fa-trash-alt" data-toggle="tooltip" title="Delete"></i></a>
                    </td>
                </tr>
                <tr>
                    <td>2018-09-29 05:57</td>
                    <td>Mobile</td>
                    <td>iPhone X 64Gb Grey</td>
                    <td><a href=""><i class="fas fa-edit" data-toggle="tooltip" title="Edit"></i></a>&emsp;
                        <a href=""><i class="fas fa-trash-alt" data-toggle="tooltip" title="Delete"></i></a>
                    </td>
                </tr>
                <tr>
                    <td>2018-09-29 05:57</td>
                    <td>Mobile</td>
                    <td>iPhone X 64Gb Grey</td>
                    <td><a href=""><i class="fas fa-edit" data-toggle="tooltip" title="Edit"></i></a>&emsp;
                        <a href=""><i class="fas fa-trash-alt" data-toggle="tooltip" title="Delete"></i></a>
                    </td>
                </tr>
            </tbody>
        </table>
</div>
@endsection