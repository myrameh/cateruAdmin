@extends('mainlayout')

@section('content')
<nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item" style="color:black"><b>Table</b></li>
          <li class="breadcrumb-item active" aria-current="page" style="color:blue;font-weight:bold">Add Table</li>
        </ol>
    </nav>
    <div class="col-12">

    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <div class="card">
                        <div class="card-body" id="form-group">
                    <div class="box-body" style="width:50%">
                        <form class="form-group form-horizontal"action={{ url('/table/addtable')}} method="post" name="add_table" id="add_table" novalidate="novalidate">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label>Capacity</label>
                                <input type="text" class="form-control" name="capacity" id="capacity"
                                value="{{ old('capacity')}}" placeholder="Capacity">
                            </div>

                            {{-- <div class="form-group">
                                <label for="" class="control-label">Category Name</label>
                                <select name="category" id="category" class="form-control" >
                                    <option value="">Select category</option>
                                    @foreach($allCategories as $category)
                                    <option value="{{ $category->categoryid}}">{{ $category->categoryname}}</option>
                                    @endforeach
                                </select>
                            </div> --}}
                            <div class="form-actions">
                                <input type="submit" value="Add SubCategory" class="btn btn-success">
                            </div>
                        </form>
                    </div>
                        </div>
                </div>
                    </div>
            </div>
        </div>
    </div>
</div>

@endsection
