<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>CaterU</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href=" {{ URL::asset('css/app.css')}}">
    <link rel="stylesheet" href="{{ URL::asset('css/custom.css')}}">
    <link rel="stylesheet" href="{{ URL::asset( 'css/bootstrap.min.css')}}" >

  </head>
<body>
<div class="wrapper">

    @include('layouts.sidebar')
    <div id="content">

        @include('layouts.header')
        @yield('content')

    </div>
</div>


<script src="{{ asset('js/jquery.slim.js')}}"></script>
<script src= "{{ asset('js/jquery.min.js')}}"></script>
<script src="{{ asset('js/moment.min.js')}}"></script>
<script src="{{ asset('js/jquery-ui.min.js')}}"></script>
<script src="{{ asset('js/popper.min.js')}}"></script>
<script src="{{ asset('js/bootstrap.min.js')}}"></script>
<script src="{{ asset('js/mdb.min.js')}}"></script>

<script>
    $(document).ready( function(){
        $('#sidebarCollapse').on('click', function () {
            $('#sidebar').toggleClass('active');
        });
    });
</script>
@yield('onchange')
@yield('js_code')
</body>
</html>
