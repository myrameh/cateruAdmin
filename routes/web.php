<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//use Illuminate\Support\Facades\URL;

Auth::routes(['register'=> false]);
Auth::routes(['login'=>false]);

Route::get('/login',function(){
    return view('create');
});
Route::post('/login',[ 'as' => 'login', 'uses' => 'LoginController@login']);
Route::post('/login/authenticate','LoginController@authenticate');
Route::get('/logout','LoginController@logout');

Route::middleware(['auth'])->group( function (){
    Route::get('/dashboard','ChartController@salesChart');
    Route::get('/menu/list', 'MenuController@listMenus');
    Route::get('/menu/addmenu','MenuController@newMenu');
    Route::get('/menu/category','MenuController@fetch');
    Route::post('/menu/addmenu', 'MenuController@saveNewMenu');
    Route::get('/menu/{menuID}/edit','MenuController@updateMenu');
    Route::post('/menu/{menuID}/edit','MenuController@saveMenuUpdate');

    Route::get('/menu/{menuID}/mark', 'MenuController@markMenu');
    Route::get('/menu/{menuID}/delete', 'MenuController@removeMenu');

    Route::get('/employee/list','EmployeeController@employeeList');

    Route::get('/admin/view_categories', 'CategoryController@categoryList');
    Route::get('/admin/add_category','CategoryController@newCategory');
    Route::post('/admin/add_category','CategoryController@addCategory');
    Route::post('/admin/add_subcategory','CategoryController@addSubCategory');
    Route::get('/admin/add_subcategory','CategoryController@newSubCategory');
    Route::get('/admin/view_subcategories', 'CategoryController@subcategoryList');
    Route::get('/admin/delete_category/{categoryid}','CategoryController@deleteCategory');//no function yet

    //TABLE
    Route::get('/table/tablelist','TableController@webTableList');
    Route::post('/table/addtable','TableController@addTable');
    Route::get('/table/addtable','TableController@newTable');
    Route::get('/order/list','OrderController@allOrderList');

    //REPORTS
    Route::get('/orders/allservedmenus','OrderDetailCOntroller@getAllServedMenusWeb');
    Route::get('/orders/successfulTransaction','OrderController@successfulTransaction');


});
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', 'Frontend\HomeController@index');

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
*/


Route::get('/createaccount', 'RegistrationController@create');
Route::post('/createaccount', 'RegistrationController@store');//





Route::get('/order/transaction','OrderController@paidOrder');

Route::get('/transaction','MainController@getTransactionByDate');
