<?php

use Illuminate\Http\Request;



Route::middleware('cors:api')->get('/login',function(){
    return view('create');
});
Route::middleware('cors:api')->post('/login',[ 'as' => 'login', 'uses' => 'LoginController@login']);
Route::middleware('cors:api')->post('/login/authenticate','LoginController@authenticate');
Route::middleware('cors:api')->get('/logout','LoginController@logout');


// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
//M E N U R O U T E S
//Route::middleware(['auth'])->group( function (){


//--------RECEPTIONIST----------//
 Route::middleware('cors:api')->get('/table/tablelist','TableController@tableList');
Route::middleware('cors:api')->get('/table/getAvailableTable','TableController@getAvailableTable');

//TABLE ORDER


//--------------KITCHEN---------------//

Route::middleware('cors:api')->get('/kitchen','KitchenController@kitchenOrders');
Route::middleware('cors:api')->get('/order/readyorder','OrderDetailController@readyOrderList');
// Route::middleware('cors:api')->post('/order/prepareMenu/{id}','KitcheController@prepareMenu');
Route::middleware('cors:api')->get('/order/readymenulist','KitchenController@getMenuReadyList');
//-------------------- S T A T U S --------------------------//
Route::middleware('cors:api')->post('/order/statusready/{id}','KitchenController@changeStatusReady');
// Route::middleware('cors:api')->post('/order/statuscook/{id}','KitchenController@cookMenu');


//----------------- CUSTOMER O R D E R R O U T E S -------------------//
Route::middleware('cors:api')->get('/menu/list', 'MenuController@ionListMenus');
Route::middleware('cors:api')->get('/order/list/{id}','OrderController@orderList');
Route::middleware('cors:api')->post('/order/startorder','OrderController@startOrder');
// Route::middleware('cors:api')->post('/order/placeorder','OrderDetailController@placeorder');
Route::middleware('cors:api')->post('/order/placeorder','CustomerController@placeorder');
Route::middleware('cors:api')->get('/order/{order_id}/edit','OrderController@editOrder');
Route::middleware('cors:api')->post('/order/{order_id}/update','OrderController@saveOrderUpdate');
Route::middleware('cors:api')->post('/order/{id}/delete','OrderController@removeOrderItem');
Route::middleware('cors:api')->get('/order/myorders/{order_id}','OrderDetailController@waitingOrderList');//TO VIEW ORDER FOR EACH CUSTOMER
Route::middleware('cors:api')->get('/order/myorders/served/{order_id}','OrderDetailController@servedOrderList');//TO VIEW ORDER FOR EACH CUSTOMER
Route::middleware('cors:api')->get('/order/status/waiting','OrderDetailController@orderStatusWaiting');

Route::middleware('cors:api')->post('/order/status/served/{id}','OrderDetailController@serveMenu');
Route::middleware('cors:api')->get('/order/servedorders','OrderDetailController@getAllServedMenus');
Route::middleware('cors:api')->get('/kitchen/completedOrders','KitchenController@readyMenu');

Route::middleware('cors:api')->get('/order/status/served','OrderDetailController@getServeMenuId');
// Route::middleware('cors:api')->post('/order/status/preparing/{id}','OrderDetailController@orderStatusPreparing');
Route::middleware('cors:api')->get('/order/allOrders','OrderDetailController@getAllOrders');
Route::middleware('cors:api')->get('/order/allOrders/{id}','OrderDetailController@getOrderByID');
Route::middleware('cors:api')->post('/order/myorders/discount/{order_id}','PaymentController@discount');
Route::middleware('cors:api')->post('/order/confirmPayment','OrderController@confirmPayment');

// Route::middleware('cors:api')->post('/order/statusserve/{id}','KitchenController@serveMenu');
Route::middleware('cors:api')->get('/order/readyList','KitchenController@getMenuReadyList');



//Route::post('/order/billout/{order_id}','CustomerController@requestBillOut');
//---------------W A I T E R------------------//
Route::middleware('cors:api')->get('/order/drinklist','WaiterController@drinklist');
Route::middleware('cors:api')->get('/order/readytoserve','WaiterController@readyToServe');
Route::middleware('cors:api')->get('/table/occupied','TableController@getOccupiedTable');
Route::middleware('cors:api')->post('/table/cleartable/{tableno}','TableController@clearTable');

///-------------BILL OUT --------------------//
Route::middleware('cors:api')->post('/cashier/sendbillinfo/{order_id}','CashierController@sendbill');

Route::middleware('cors:api')->get('/cashier/billOutList','CashierController@getBillOutList');// Show all billout
Route::middleware('cors:api')->get('/cashier/getbillinfo/{order_id}','CashierController@getbilldetail');//show details per table//
Route::middleware('cors:api')->post('/cashier/setTotal/{order_id}','PaymentController@setTotal');

// Route::middleware('cors:api')->post('/cashier/sendbillinfo/{order_id}','CashierController@updateTotal');
Route::middleware('cors:api')->post('/cashier/confirmPayment/{order_id}','PaymentController@confirmPayment');
Route::middleware('cors:api')->post('/cashier/printReceipt/{order_id}','PaymentController@printReceipt');

// Route::get('/customer/addNCustomer','CustomerController@newCustomer');
// Route::post('/customer/addNCustomer','CustomerController@addNCustomer');

// Route::post('/concern/store/{tableno}','TemporaryTableController@storeConcern');

Route::middleware('cors:api')->get('/get/sum/{menuID}','OrderDetailController@sumOrderQty');
