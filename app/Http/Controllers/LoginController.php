<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;

use App\AppSettings;
use App\Employee;
class LoginController extends Controller
{
    public $successStatus = 200;
    protected $redirectTo= '/login';


    public function username(){
        return 'username';
    }

    public function login(){
        if(Auth::attempt(['username' => request('username'), 'password' => request('password')])){
            $user = Auth::user();
            $success['token'] =  $user->createToken('MyApp')-> accessToken;
            return response()->json(['success' => $success], $this-> successStatus);
        }
        else{
            return response()->json(['error'=>'Unauthorised'], 401);
        }
    }
    public function authenticate(Request $request){

        if(auth()->attempt(request(['username','password']))==false){
            return back()->withErrors([
                'message' => 'The username or password is incorrect, please try again'
            ]);
        }
        if(Auth::user()->position == 'waiter'){
            return redirect()->to(url('/menu/list?mode=list'));
        }

    }

    public function logout(){
        Auth::logout();
         return redirect()->to(url('/login'));
    }

}
