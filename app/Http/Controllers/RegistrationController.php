<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\AppSettings;
use App\Employee;
class RegistrationController extends Controller
{
    public function create(){
        return view('users.createprofile');
    }

    public function store(Request $request)
    {
        $this->validate(request(), [
            'empfirstname' => 'required',
            'emplastname'  => 'required',
            'username'  => 'required|unique:employees',
            'position'  => 'required',
            'password'  => 'required'

        ]);
        
        $password = bcrypt($request->password);
        $user = new Employee;

        $user->empfirstname = $request->empfirstname;
        $user->emplastname  = $request->emplastname;
        $user->username = $request->username;
        $user->position = $request->position;
        $user->password = bcrypt($request->password);
        $user->save();

        auth()->login($user);

        return redirect()->to('/createaccount');
    }
}
