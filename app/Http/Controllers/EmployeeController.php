<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
class EmployeeController extends Controller
{
    public function employeeList(){
        $lists = Employee::all();

        return view('admin.employeelist',compact('lists'));
    }
}
