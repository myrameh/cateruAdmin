<?php

namespace App\Http\Controllers;
use App\Http\Controllers\BaseController as BaseController;
use Illuminate\Http\Request;
use App\Category;
use App\SubCategory;
use DB;
class CategoryController extends BaseController
{
    public function getCategoryName(){
        $allCategories= Category::all();
        $allSubs = SubCategory::all();
        return response()->json([
            'allCategories' => $allCategories,
            'allSubs'   => $allSubs,
        ]);
    }
    public function newCategory(){
        $allCategories = Category::all();

        return view('admin.category.add_category',compact('allCategories'));
    }
    public function newSubCategory(){

        $allSubCategories = SubCategory::all();
        $allCategories  = Category::all();
        return view('admin.category.add_subcategory',compact('allSubCategories','allCategories'));
    }
    public function categoryList(Request $request){
        $allCategories = Category::all();
            return view('admin.category.categorylist', compact('allCategories'));


    }
    public function subcategoryList(Request $request){
        $allSubCategories = SubCategory::all();
        $allCategories = Category::all();
            return view('admin.category.subcategorylist', compact('allSubCategories','allCategories'));


    }
    public function addCategory(Request $request){
        /**if mmode = add */
            $category = new Category;
            $category->categoryname = $request->categoryname;
            $category->description = $request->description;
            $category->save();

       return redirect()->to('/admin/view_categories');
       /** if mode = select */
    }

    public function editCategory(Request $request, $id=null){
       if($request->isMethod('post')){
           $data= $request->all();

           Category::where(['categoryid'=>$id])
            ->update(['categoryname'=>$data['categoryname'],'description' => $data['description'],'url' => $data['url']]);

           return redirect('/admin/view-categories')->with('flash_message_success','Category updated successfully!');
       }
       $categoryDetails = Category::where(['categoryid'=> $id])->first();
       return view('admin.category.edit_category')->with(compact('categoryDetails'));
    }
    public function deleteCategory($category_id)
    {
        $category = Category::find($category_id);
        if($category){
            $category->delete();
        }
        return redirect()->to('/admin/view_categories');
    }

    public function addSubCategory(Request $request){

        $sub = new SubCategory;
        $sub->subname = $request->subname;
        $sub->categoryid = $request->categoryid;
        $sub->save();

   return redirect()->to('/admin/category/subcategoryl');
}
}
