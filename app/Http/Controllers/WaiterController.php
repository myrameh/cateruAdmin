<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OrderDetail;
use App\Menu;
use App\Category;
use App\SubCategory;
use App\Order;
class WaiterController extends Controller {
    public function drinklist(){
        $allOrders = OrderDetail::all();
        $allCategories = Category::all();
        $drinks=array();
        $result = array();
       foreach($allOrders as $orders){
           foreach($orders->order as $ords){
              // foreach($allCategories as $category){

            $order = Order::find($orders->order_id);
            $menus = Menu::find($orders->menuID);
            $menus->subCategory();
            $sub = SubCategory::find($menus->subcatid);
            $cat = $sub->category();
            $catid = $menus->subcategory->categoryid;

            if($orders->status == 'waiting' && $catid == 11 ){
                array_push( $drinks,array(
                    'order_id' => $order->order_id,
                    'tableno'=> $order->tableno,
                    'name' => $menus->name,
                    'menu_id' => $menus->menuID,
                    'detail_id' => $orders->id,
                    'quantity' => $orders->orderQty
                ));

            }
        }
      //  }
        }

        foreach ($drinks as $element=> $value) {
            $result[$value['order_id']][] = $value;
        }
        return response()->json([

            'result' => $result
        ]);
    }
    // public function serveDrink($id){
    //     $order = OrderDetail::find($id);
    //     $order->status ='Served';
    //     $order->save();

    //     return response()->json([
    //         'message' => 'Drink is served!'
    //     ]);
    // }

    // public function readyToServe(){
    //     $orders = OrderDetail::all();
    //     $readylist = array();
    //     $items = array();

    //     foreach($orders as $order){
    //         $menus = Menu::find($order->menuID);
    //         $orders = Order::find($order->order_id);

    //         if($order->status == 'Ready'){
    //             array_push($readylist, array(
    //                 'order_id' => $order->order_id,
    //                 'list' => array(
    //                     'tableno'=> $orders->tableno,
    //                     'name' => $menus->name,
    //                     'menu id' => $menus->menuID,
    //                     'quantity' =>$order->orderQty
    //                     )
    //             ));
    //         }
    //     }
    //     return response()->json([
    //         'list' => $readylist
    //     ]);
    // }

    public function readyToServe(){
        $allOrders = OrderDetail::all();
        $allCategories = Category::all();
        $list=array();
        $result = array();
        foreach($allOrders as $orders){
            foreach($orders->order as $ords){
                $order = Order::find($orders->order_id);
                $menus = Menu::find($orders->menuID);
                $menus->subCategory();
                $sub = SubCategory::find($menus->subcatid);
                $sub->category();
                $catid = $menus->subcategory->categoryid;

                if($orders->status == 'Ready'){
                    array_push( $list,array(
                        'order_id' => $order->order_id,
                        'tableno'=> $order->tableno,
                        'name' => $menus->name,
                        'menu_id' => $menus->menuID,
                        'detail_id' => $orders->id,
                        'quantity' => $orders->orderQty
                    ));
                }
            }
        }

        foreach ($list as $element=> $value) {
            $result[$value['order_id']][] = $value;
        }
        return response()->json([

            'result' => $result
        ]);
    }
    public function servedDrinkList(){
      $drinks = DB::table('order_details')
                    ->where('status','Served')
                    ->where('menuID','1001');
    }
}
