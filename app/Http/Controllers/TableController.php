<?php

namespace App\Http\Controllers;
use App\Http\Controllers\BaseController as BaseController;
use Illuminate\Http\Request;
use App\RestaurantTable;
use App\Order;
use DB;
class TableController extends BaseController
{
    public function tableList(){
       $allTables = RestaurantTable::all();
        return response()->json(['allTables'=> $allTables]);
    }

    public function webTableList(){
        $allTables = RestaurantTable::all();
         return view('admin.tablelist',compact('allTables'));
     }

    public function getAvailableTable(){
        $availableTables = DB::table('tables')->where('status','Available')->get();

        return response()->json([
            'AvailableTables' => $availableTables
        ]);
    }
    public function getOccupiedTable(){
        $occupiedTables = DB::table('tables')->where('status','Occupied')->get();

        return response()->json([
            'OccupiedTables' => $occupiedTables
        ]);
    }

    public function newTable(){


        return view('tables');

    }

    public function addTable(Request $request){
        $table = new RestaurantTable;

        $table->capacity = $request->capacity;
        $table->status = 'Available';

        $table->save();

        return redirect()->to(url('/table/tablelist'));
    }
    public function requestTableTransfer($order_id,Request $request){
        $table = Table::find($request->tableno);
        if($table->status == 'occupied'){
            return response()->json([
                'error_message' => 'The table'.$table.'is not Avaible'
            ]);
        }else{
            $order = Order::find($order_id);
            $order->tableno = $table->tableno;
            $order->save();

            return response()->json([
                'message' => 'Table transfer is successful!'
            ]);
        }
    }

    public function tableTransfer($order_id,Request $request){
        $orders=Order::find($order_id);
        $table=RestaurantTable::find($request->tableno);

        if($table->tableno == "Available"){
            DB::table('orders')->where('order_id',$order_id)
            ->update(['tableno' => $table]);
            }
        }

    public function clearTable(Request $request){
        $table = RestaurantTable::find($request->tableno);
        $table->status = 'Available';
        $table->save();

        return response()->json([
            'message' => 'Table Cleared'
        ]);
    }


}
