<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OrderDetail;
use App\Order;
use App\Menu;
use DB;
class CashierController extends Controller
{
    public function getbilldetail($order_id){
        $orderRecord = Order::find($order_id);
        $orderdetails=OrderDetail::all();

        $records = array();
        foreach($orderdetails as $detail){
            foreach($detail->menu as $menu){
                if($detail->order_id == $order_id){
                    array_push($records,
                    array(
                        'menuname' => $menu->name,
                        'price' => $menu->price,
                        'quantity'  => $detail->orderQty,
                        'subtotal'  => $detail->subtotal,
                    ));
                }
            }
        }

      return response()->json([
            'order_records' => $records,
            'total'  => $orderRecord->total,
            'tableno'=> $orderRecord->tableno,
            'orderRecords' => $orderRecord
        ]);
    }
    public function updateTotal($order_id,Request $request){

        $orders = Order::find($order_id);
        $orders->total = $request->total;
        $orders->save();

        return response()->json([
            'message' => 'Bill sent'
        ]);

    }

    public function getBillOutList(){
         $orders = DB::table('orders')->where('status','billout')->get();

          return response()->json([
             'table' => $orders
          ]);
    }

}
