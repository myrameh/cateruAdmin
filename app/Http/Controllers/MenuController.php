<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Response;
use App\Http\Controllers\BaseController as BaseController;
use App\Menu;
use Validator;
use App\Category;
use App\SubCategory;
use DB;
class MenuController extends BaseController
{

//WEB ROUTES
    public function dashboard(){
         return view('pages.dashboard');
    }
    public function newMenu(){
        $allMenus = Menu::all();
        $allSubCategories = SubCategory::all();
        $allCategories = Category::all();

        return view('pages.addmenu', compact('allMenus', 'allCategories', 'allSubCategories'));
    }

    public function fetch(Request $request){
        $categoryid = Input::get('categoryid');
        $subcategories = DB::table('sub_categories')
            ->where('categoryid',$categoryid)->get();

            return response()->json(['subs' => $subcategories]);
    }
    public function saveNewMenu(Request $request){

        $filename = $request->file('image')->getClientOriginalName();

        $path = public_path().'/menu/menu_images';
        $request->file('image')->move($path, $filename);

        $newMenu = new Menu();

        $newMenu->menuID = $request->menuID;
        $newMenu->name = $request->name;
        $newMenu->details = $request->details;
        $newMenu->price = $request->price;
        $newMenu->servingsize = $request->servingsize;
        $newMenu->image = $filename;
        $newMenu->subcatid  = $request->subcategory;

        $newMenu->save();
       return redirect('/menu/list?mode=list');

        //else
        //     return redirect()->back()->withInput()->withErrors($validation);
    }

    public function updateMenu($menuID){
        $allMenus = Menu::all();
        $allSubCategories = SubCategory::all();
        $allCategories = Category::all();
        $menuRecord = Menu::find($menuID);


        return view('pages.updatemenu', compact('menuRecord', 'allMenus', 'allSubCategories', 'allCategories'));
    }
    public function saveMenuUpdate( $menuID,Request $request)
    {
        $filename = $request->file('image')->getClientOriginalName();

        $path = public_path().'/menu/menu_images';
        $request->file('image')->move($path, $filename);


        $menuRecord = Menu::find($menuID);

        $menuRecord->name = $request->name;
        $menuRecord->details=$request->details;
        $menuRecord->price = $request->price;
        $menuRecord->servingsize = $request->servingsize;
        $menuRecord->image = $filename;
        $menuRecord->subcatid = $request->subcatid;


        $menuRecord->save();

        return redirect()->to(url('/menu/list?mode=list'));
        // }
        // else
        //     return redirect()->back()->withInput()->withErrors($validation);
    }

    public function listMenus(Request $request) // show al menu list
    {
        $allMenus = Menu::all();

        if($request->mode == 'list'){
            return view('pages.menulist', compact('allMenus'));
        }
        else if ($request->mode == 'remove') {
            $menuRecords = Menu::find($request->menuID);

            if ($menuRecords) {
                $menuID = $menuRecords->menuID;
                $menuName =  $menuRecords->menuName;
                $details = $menuRecords->details;
                $price = $menuRecords->price;
                $servingsize=$menuRecords->servingsize;
                $subcatid = $menuRecords->subcatid;
                $image = $menuRecords->image;

            }
            else {
                $menuID = null;
                $menuNname =  null;
                $details =null;
                $price = null;
                $servingsize=null;
                $subcatid =  null;
                $image = null;
            }

            return view('pages.markmenu', compact('menuID','menuName','details','price','servingsize','subcatid','image','allMenus'));
        }
    }
    public function markMenu($menuID)
    {
        return redirect()->to(url('/menu/list?mode=remove&menuID=').$menuID);
    }

    public function removeMenu($menuID)
    {
        $menuRecord = Menu::find($menuID);

        if ($menuRecord) {
            $menuRecord->delete();
        }

        return redirect()->to(url('/menu/list?mode=list'));
    }


    public function ionNewMenu(){
        $allMenus = Menu::all();
        $allCategories = Category::all();
        $allSubCategories = SubCategory::all();
        return $this->sendResponse($allMenus->toArray(), 'Menu retrieved successfully.');
    }
    public function ionListMenus(Request $request)
    {
        $allMenus = Menu::all();
        $menus = array();
        $result = array();
        foreach($allMenus as $menu){
            $path = public_path().'/menu/menu_images/'.$menu->image;

            array_push($result, array(
                'images' => $path,
                'menuID'    => $menu->menuID,
                'name'  => $menu->name,
                'details' => $menu->details,
                'servingsize' => $menu->servingsize,
                'price' => $menu->price,
                'subcatid' => $menu->subcatid

            ));
        }
        if($request->mode == 'list'){
           return  response()->json([
               'allMenus' => $allMenus,
               'result' => $result
               ]);
        }

    }
    // public function ionRemoveMenu($menuID) // remove marked employee from the table
    // {
    //     $menuRecord = Menu::find($menuID);

    //     if ($menuRecord) {
    //         $menuRecord->delete();
    //     }

    //     return $this->sendResponse($menuRecord->toArray(),'Menu deleted successfully!');
    // }


    // public function ionSaveNewMenu(Request $request)
    // {
    //     // parse_str($request->getContent(), $data);
    //     dd($request->getContent());

    //     return response()->json([
    //         'menuID' => 1
    //     ]);
    // }
    // public function getTransactionByDate($from_date,$to_date){
    //     DB::table('order_details')
    //             ->where('status','served')
    //             ->whereBetween('created_at',[$from_date,$to_date])->get();

    //     return response()->json([
    //         'message' => 'data returned'
    //     ]);
    // }


}
