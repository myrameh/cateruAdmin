<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\AppSettings;
use App\Customer;
use App\Order;
use App\OrderDetail;
use App\RestaurantTable;
use DB;

class CustomerController extends Controller
{

    public function newCustomer(){
        $allCustomers = Customer::All();

        return response()->json([
            'allCustomers' => $allCustomers
        ]);
    }
    public function addCustomer(Request $request){

        Customer::create($request->all());
        return response()->json([
            'message' => 'Customer added'
        ]);
    }
    public function requestBillOut($order_id){

        $orderid = Order::find($order_id);
        return response()->json([
            'orderid' => $orderid->order_id,
            'tableno'  => $orderid->tableno
            //'discount'  => $discount
            ]);
    }
    public function placeorder(Request $request){

        $data = $request->all();
        $finalArray = array();

        foreach($data as $key=>$value){
            array_push($finalArray,array(
            'order_id' => $value['order_id'],
            'orderQty' => $value['orderQty'],
            'menuID' =>  $value['menuID'],
            'status' => $value['status'],
            'subtotal' => $value['subtotal'] )
            );
        };
        OrderDetail::insert($finalArray);

        return response()->json([
            'message'=> 'Order successful!',
            'final' => $finalArray
        ]);
    }
    public function editOrder($order_id){
        $allMenus = Menu::all();
        $allOrders = OrderDetail::find($order_id);

        return response()->json([
            'allMenus' => $allMenus,
            'allOrders' => $allOrders
        ]);
    }


}
