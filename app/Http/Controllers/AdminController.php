<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function showMenuListByDate(Request $request){
       $from = $request->from;
       $to = $request->to;

       $lists = DB::table('order_details')
                    ->whereBetween('created_at',[$from,$to])->get();

        return response()->json([
            'lists' => $lists
        ]);
    }
}
