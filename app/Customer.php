<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $primaryKey = 'custid';
    protected $table = 'customers';
    public $incrementing = true;
    public $timestamps = false;

    protected $fillable = array('custid','name','phonenumber');
    
    public function order(){
        return $this->belongsTo('App\Order','custid');
    }
}
