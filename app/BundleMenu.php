<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BundleMenu extends Model
{
    protected $table = 'bundle_menus';
    protected $primaryKey = 'bundleid';
    public $incrementing= true;
    public $timestamps = true;
}
