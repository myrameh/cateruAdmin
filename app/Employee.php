<?php
namespace App;

use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticatableTrait;

class Employee extends Model implements Authenticatable
{
    
    use  AuthenticatableTrait;

    protected $primaryKey = 'username';
    protected $table = 'employees';
    public $timestamps = false;
    public $imcrementing = false;

    protected $fillable =array('empfirstname','emplastname','username','position','password');
}
